import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class ForecastResult extends Component {
    render() {
        return (
            <div style={{ background: 'white', margin: '25px', borderRadius: '15px' }}>
                
                {this.props.forecast.current !== undefined &&
                    <div>
                            <img src={this.props.forecast.current.weather_icons[0]}
                                alt={this.props.forecast.current.weather_descriptions[0]}
                                style={{ margin: '20px' }} />
                       
                            <p style={{
                                fontFamily: 'Montserrat',
                                fontSize: '22px',
                                color: '#404491'
                            }}>{this.props.forecast.current.weather_descriptions}</p>
                       
                    </div> }
                <div>
                    {this.props.forecast.current !== undefined &&
                        <p style={{
                            fontFamily: 'Montserrat',
                            fontSize: '64px',
                            color: '#404491'
                        }}>{this.props.forecast.current.temperature} °C</p>
                    }
                </div>
                <div style={{ background: '#404491', borderRadius: '5px', color:'white' }}>
                   
                    {this.props.forecast.current !== undefined &&
                        <div><img src="./Wind.png" />
                                {this.props.forecast.current.wind_speed} km/h</div>
                    }
                
                    {this.props.forecast.current !== undefined &&
                        <div><img src="./RainSmall.png" />
                                {this.props.forecast.current.humidity} %</div>
                    }
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
