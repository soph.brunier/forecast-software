import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchForecast } from '../actions/forecast';
import ForecastChooseCity from './ForecastChooseCity';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';

class Forecast extends Component {
    render() {
        return (
            <div>
                <div>
                {this.props.forecast.current !== undefined &&
                    <ForecastTitle city={this.props.city} />
                }
                </div>
                <div>
                    < ForecastResult />
                </div>
                <div>
                    <ForecastChooseCity city={this.props.city} onClick={() => { this.props.fetchForecast(this.props.city) }} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast,
        city: state.forecast.city
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast,
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Forecast);
