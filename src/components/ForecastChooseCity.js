import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateCity } from '../actions/forecast';

class ForecastChooseCity extends Component {
    render() {
        return (
            <div >
                <form
                    placeholder="City"
                    aria-label="City"
                    aria-describedby="basic-addon2"
                    
                />
                    <input className="mb-3" style={{ width:'300px'}}
                        value={this.props.city}
                        onChange={(event) => { this.props.updateCity(event.target.value) }}/>
                    <button variant="outline-secondary" onClick={this.props.onClick}>
                        <img src="./search.png" style={{ width:'25px', height:'25px'}} />
                    </button>
            
                {this.props.loader &&
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loader: state.forecast.loader
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateCity
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastChooseCity);